import numpy as np

colour_csv_path = '../data/colour_list.csv'


# all colour titles are given a corresponding integer value
# this function takes in a colour title and returns the
# corresponding integer
def colour_to_int(colour):
    colour = colour.lower()

    if colour == 'green':
        return 0
    elif colour == 'yellow':
        return 1
    elif colour == 'orange':
        return 2
    elif colour == 'brown':
        return 3
    elif colour == 'red':
        return 4
    elif colour == 'pink':
        return 5
    elif colour == 'purple':
        return 6
    elif colour == 'blue':
        return 7
    elif colour == 'navy':
        return 8
    elif colour == 'black':
        return 9
    elif colour == 'white':
        return 10
    elif colour == 'gray':
        return 11
    elif colour == 'darkgray':
        return 12


# This function takes in an integer and returns
# the corresponding colour's title
def int_to_colour(integer):
    if integer == 0:
        return 'green'
    elif integer == 1:
        return 'yellow'
    elif integer == 2:
        return 'orange'
    elif integer == 3:
        return 'brown'
    elif integer == 4:
        return 'red'
    elif integer == 5:
        return 'pink'
    elif integer == 6:
        return 'purple'
    elif integer == 7:
        return 'blue'
    elif integer == 8:
        return 'navy'
    elif integer == 9:
        return 'black'
    elif integer == 10:
        return 'white'
    elif integer == 11:
        return 'gray'
    elif integer == 12:
        return 'darkgray'


# we consider shades as black, white, gray, darkgray etc.
def is_shade(name):
    if name == 'darkgray' or name == 'gray' or name == 'black' or name == 'white' or name == 'navy':
        return True
    else:
        return False


# returns the distance between two points in 3d space
def get_distance(p1, p2):
    return np.sqrt(np.sum((p1 - p2) ** 2, axis=0))


# reads the colour data from file into a list containing dictionaries
def colour_definitions():
    colours = []
    with open(colour_csv_path, 'r') as f:
        for i, line in enumerate(f):
            if i > 0:  # skip first line because it only contains headings
                line_data = line.split(',')

                name = line_data[0]
                dominant = colour_to_int(line_data[1])
                recessives = []
                for item in line_data[2].split('-'):
                    recessives.append(colour_to_int(item))
                rgb = np.array([int(line_data[3]), int(line_data[4]), int(line_data[5])])

                colours.append({'name': name,
                                'dominant': dominant,
                                'recessives': recessives,
                                'rgb': rgb})

    return colours


# takes in an rgb colours and returns the
# dominant and recessive colours for that
def get_colour_profile(rgb, colour_defs):
    min_distance = 9999
    for colour in colour_defs:
        distance = get_distance(rgb, colour['rgb'])

        if distance < min_distance:
            min_distance = distance
            dominant = colour['dominant']
            recessives = colour['recessives']

    return dominant, recessives


# calculates the dominant and recessive colours at every possible csv
# value to form a lookup table so that this doesn't need to be calculated
# at run time
# this solution has not actually been implemented yet
def generate_colour_matrix():
    colour_matrix = []
    for i in range(256):
        colour_matrix.append([])
        for j in range(256):
            colour_matrix[i].append([])
            for k in range(256):
                colour_matrix[i][j].append(get_colour_profile(np.array([i, j, k])))

    return colour_matrix


# takes in two colour names and their ratio and returns
# what colour it suggests they should make up
# ratio is the frequency of colour2 / colour1
def extract_hist(colour_hist):
    colour_names = ['green', 'yellow', 'orange', 'brown', 'red', 'pink', 'purple', 'blue', 'navy', 'black', 'white',
                    'gray', 'darkgray']
    colour_dict = {}
    for i, colour in enumerate(colour_names):
        colour_dict[colour] = int(colour_hist[i])

    colour_dict = sorted(colour_dict.items(), key=lambda kv: kv[1])  # Sort the dictionary by value

    # make a dictionary that doesn't include the shade colours: grays, black, white
    non_shade_dict = []
    for item in colour_dict:
        if not is_shade(item[0]):
            non_shade_dict.append(item)

    # a set of rules that determine if and what secondary and third colours should be returned
    # e.g. add second most dominant colour on histogram if it occurs in the image more than 70%
    #      that of the first most dominant colour
    colours = [colour_dict[-1][0]]
    if not is_shade(colours[0]):
        if colours[0] == 'brown' and non_shade_dict[-2][0] == 'yellow':  # special case for brown and yellow
            if non_shade_dict[-2][1] >= 0.25 * non_shade_dict[-1][1]:
                colours.append('yellow')
                if not colour_dict[-2][0] == 'yellow' and colour_dict[-2][1] >= 0.7 * colour_dict[-1][1]:
                    colours.append(colour_dict[-2][1])
        elif colour_dict[-2][1] >= 0.70 * colour_dict[-1][1]:
            colours.append(colour_dict[-2][0])
            if colour_dict[-3][1] >= 0.85 * colour_dict[-2][1]:
                colours.append(colour_dict[-3][0])
    else:
        if not is_shade(colour_dict[-2][0]):
            if colour_dict[-2][1] > 0.4 * colour_dict[-1][1]:
                colours.append(colour_dict[-2][0])
                if colour_dict[-3][1] >= 0.85 * colour_dict[-2][1]:
                    colours.append(colour_dict[-3][0])
        elif colour_dict[-2][1] >= 0.55 * colour_dict[-1][1]:
            colours.append(colour_dict[-2][0])
            if not is_shade(colour_dict[-3][0]) and colour_dict[-3][1] >= 0.85 * colour_dict[-2][1]:
                colours.append(colour_dict[-3][0])

    return colours

