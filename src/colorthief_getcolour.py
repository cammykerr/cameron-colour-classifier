import cv2
import os
from colorthief import ColorThief

image_directory_path = '../data/set_2'


def crop_center_100x100(img):
    h, w, _ = img.shape  # get width and height
    mid_y = int(h / 2)
    mid_x = int(w / 2)

    return img[mid_y - 50:mid_y + 50, mid_x - 50: mid_x + 50]


def crop_set2(img):
    h, w, _ = img.shape
    mid_y = int(h * 2.85 / 4)
    mid_x = int(w / 2)

    return img[mid_y - 175:mid_y + 175, mid_x - 175: mid_x + 175]


def get_average_colour(img):
    totalPix = 0
    r = 0
    g = 0
    b = 0

    for row in img:
        for pix in row:
            totalPix += 1
            r += pix[2]
            g += pix[1]
            b += pix[0]

    return int(r/totalPix), int(g/totalPix), int(b/totalPix)


if __name__ == '__main__':
    for image_file in os.listdir(image_directory_path):
        image_path = image_directory_path + '/' + image_file
        img = cv2.imread(image_path)

        # img = crop_center_100x100(img)
        img = crop_set2(img)
        cv2.imwrite('temp.jpg', img)

        color_thief = ColorThief('temp.jpg')
        dominant_color = color_thief.get_color(quality=1)

        print('color_thief: \t\t' + str(dominant_color))
        print('average_colour: \t' + str(get_average_colour(img)) + '\n')

        cv2.imshow(str(dominant_color), img)
        cv2.waitKey(0)

    os.remove('temp.jpg')




