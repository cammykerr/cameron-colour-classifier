import os
import sys
import cv2
from opencv_getcolour import colour_wrapper_path, plot_colour_hist
from colour import extract_hist

if __name__ == '__main__':
    if len(sys.argv) == 2:
        path = sys.argv[1]
        for file in os.listdir(path):
            colours, hist, im = colour_wrapper_path(path + '/' + file, 2, False)

            cv2.imshow(str(colours), im)
            plot_colour_hist(hist)
            cv2.waitKey(0)
