""" Just a script I used to transform the colour list file """

with open('colour_list.csv', 'r') as f:
    lines = f.readlines()

with open('many_colours.csv', 'w') as f:
    for i, line in enumerate(lines):
        line = line.replace('\n', '')
        line = line.replace('[', '')
        line = line.replace(']', '')

        if i == len(lines) - 1:
            end = ''
        else:
            end = '\n'

        f.write(line + end)