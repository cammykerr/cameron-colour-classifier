From opencv_getcolour.py you want to import the functions colour_wrapper_path() or colour_wrapper()

FUNCTION 1:
colour_wrapper_path()

Description: Takes in a path to an image, and determines the colour/colours of that image

Arguments:
    path: path to an image
    crop_method (1,2 or 3): determines where the image is cropped, with 1 being the center 100x100, 2 being a specific
                            location on images where the roborigger load is usually visible, 3 is no crop
    is_url (optional): set as True if the path is a url, set as false if the path is local

Returns:
    colours: a list of the determined dominant colours
    histogram: a histogram of all the colour frequencies
    image: the cropped image

FUNCTION 2:
colour_wrapper()

Description: Same functionality but takes in only one argument: a BGR image. Crop or resize to about 100x100 before 
using this function, otherwise it will take a long time to process. If the image is not BGR you can use the function 
cv2.cvtColor(load_img, cv2.COLOR_RGB2BGR)

Arguments:
    bgr_image: 

Returns:
    colours:a list of the determined dominant colours
    histogram: a histogram of all the colour frequencies

FUNCTION 3:
plot_colour_hist()

Description: Call this function taking in the returned histogram from the colour_wrapper_path() or colour_wrapper()
functions. This will plot a histogram of the colours found in the image.

Arguments: colour_profile, an array of size 13 where each element is the frequency a particular colour occurs in an
image. This variable is returned by the colour_wrapper_path() and colour_wrapper() functions.